package com.example.sharedpreferance

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferances: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        sharedPreferances = getSharedPreferences("userInfo", Context.MODE_PRIVATE )

    }

    fun save(view: View) {

        val eMail = emailET.text.toString()
        val firstName = firstnameET.text.toString()
        val lastName = lastnameET.text.toString()
        val age = ageET.text.toString().toInt()
        val address = addressET.text.toString()


        if (eMail.isEmpty() || lastName.isEmpty() || firstName.isEmpty() || age==0 || address.isEmpty()) {

            val edit = sharedPreferances.edit()
            edit.putString("E-Mail", eMail)
            edit.putString("First name", firstName)
            edit.putString("Last name", lastName)
            edit.putInt("Age", age)
            edit.putString("Address", address)
            edit.apply()
        }
        else if (eMail.isNotEmpty() && lastName.isNotEmpty()  && firstName.isNotEmpty() && age != 0 && address.isNotEmpty()){
            read()
        }
    }

    fun read(view: View){
        val eMail = sharedPreferances.getString("E-mail","")
        val firstname = sharedPreferances.getString("First name","")
        val lastname = sharedPreferances.getString("Last name","")
        val age = sharedPreferances.getInt("Age",0)
        val address = sharedPreferances.getString("Address","")

    }
}